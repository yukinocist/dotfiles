"
" $ .vimrc version 1.0 2017/06/24 yukinocist $
" 

set nocompatible
set backspace=indent,eol,start
set number
set tabstop=4
set ruler
set encoding=utf-8
set fileencoding=utf-8
set noswapfile
set nowritebackup
set nobackup
set hlsearch
set ignorecase
set smartcase
set autoindent

inoremap <C-e> <Esc>$a
inoremap <C-a> <Esc>^i
inoremap <C-h> <LEFT>
inoremap <C-j> <DOWN>
inoremap <C-k> <UP>
inoremap <C-l> <Right>
inoremap <C-w> <Esc>lwi
inoremap <C-b> <Esc>bi
inoremap <C-c> <Esc>
inoremap <> <><Left>
inoremap () ()<Left>
inoremap {} {}<Left>
inoremap [] []<Left>
inoremap "" ""<Left>
inoremap '' ''<Left>
inoremap <C-x><C-s> <ESC>:wq!<enter>
inoremap <C-x><C-k> <ESC>:q!<ENTER>
noremap <C-x><C-s> <ESC>:wq!<ENTER>
noremap <C-x><C-k> <ESC>:q!<ENTER>
